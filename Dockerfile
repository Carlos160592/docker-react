FROM node:16-alpine as builder
WORKDIR '/app'
COPY package.json .
RUN npm install -g npm@8.5.2
RUN npm install -g create-react-app
RUN npm install --save react react-dom 

FROM nginx
EXPOSE 80
COPY --from=builder /app/build /usr/share/nginx/html